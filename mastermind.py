
from dataclasses_json import dataclass_json
from dataclasses import dataclass

import argparse

from datetime import datetime
from enum import Enum
from typing import List
import os, sys
import random
import uuid

import getch

from graphics import print_code_colors, print_row, print_game, printProgressBar
from models import Agent, Ruleset, Game, Response
APPLICATION_DATA_DIR = os.path.join(os.environ['HOME'], '.local', 'share', 'mastermind')

GUESS_CLEAR_CHARACTER = 'q'

def uid() -> str:
    return str(uuid.uuid4())



@dataclass_json
@dataclass
class HumanAgent(Agent):

    def guess(self, game: 'Game') -> List[int]:
        print_code_colors()
        print_game(game, print_code=False)
        guess = []
        while len(guess) < game.rules.num_slots:
            c = getch.getch()
            if c == GUESS_CLEAR_CHARACTER:
                guess = []
            else:
                try:
                    color_index = int(c)
                    if color_index >= 0 and color_index < game.rules.num_colors:
                        guess.append(color_index)
                except ValueError:
                    continue
            print_row(guess, end='\r')
        print()
        return guess


def _norm(proba: List[float]) -> List[float]:
    mag = sum(proba)
    return [x/mag for x in proba]


class RandomAgent(Agent):


    def guess(self, game: 'Game') -> List[int]:
        return [ random.randint(0, game.rules.num_colors - 1) for i in range(game.rules.num_slots) ]


class BaselineAgent(Agent):

    def __init__(self, rules, name):
        self.name = name
        uniform_proba = 1.0 / rules.num_colors
        def jitter(magnitude: float) -> float:
            if random.random() <= 0.5:
                return -1 * random.random() * magnitude
            else:
                return random.random() * magnitude

        self.slot_probabilities = [[uniform_proba + jitter(0.002) for j in range(rules.num_colors)] for i in range(rules.num_slots)]
        self._normalize()

    def guess(self, game: 'Game') -> List[int]:
        self.update(game)
        return [slot_proba.index(max(slot_proba)) for slot_proba in self.slot_probabilities]

    def _normalize(self):
        


        self.slot_probabilities = [_norm(sp) for sp in self.slot_probabilities]

    def _update(self, game: 'Game'):
        priors = self.slot_probabilities
        probas = []



def get_response(guess: List[int], code: List[int]) -> Response:
    if len(guess) != len(code):
        raise ValueError(f'Guess is length {len(guess)} but code is length {len(code)}')

    num_red = 0
    num_white = 0
    used_indices = []
    for i in range(len(guess)):
        if guess[i] == code[i]:
            num_red += 1
            used_indices.append(i)

    for i in range(len(guess)):
        if guess[i] == code[i]:
            continue
        
        for j in range(len(guess)):
            if guess[i] == code[j] and j not in used_indices:
                num_white += 1
                used_indices.append(j)
                break
        
    return Response(red_pins=num_red, white_pins=num_white, empty_pins=len(guess) - (num_red + num_white))
    

def play_game(game: Game, render: bool = True) -> bool:
    while game.code not in game.guesses and len(game.responses) < game.rules.num_guesses:
        if render:
            os.system('clear')
        guess = game.agent.guess(game)
        response = get_response(guess, game.code)
        game.guesses.append(guess)
        game.responses.append(response)
    
    if render:
        print_game(game)

        print('Code was:\n')
        print_row(game.code)
        print()

    if game.code in game.guesses:
        if render:
            print('Congrats you won!')
        return True
    else:
        if render:
            print('You lost haha')
        return False

def run_simulation(num_games, rules: Ruleset, agent: Agent, savedir: str, render: bool):
    wins = 0
    printProgressBar(0, num_games, prefix = 'Progress:', suffix = 'Complete', length = 80)
    start = datetime.now()
    for i in range(num_games):
        g = init_game(agent, rules)
        won_game = play_game(g, render)
        if won_game:
            wins += 1
        g.save(savedir)
        printProgressBar(i+1, num_games, prefix = 'Progress:', suffix = 'Complete', length = 80)
    end = datetime.now()
    print()
    sim_seconds = (end - start).total_seconds()
    games_per_second = round(num_games/sim_seconds, 2)
    print(f'Simulation time: {sim_seconds} seconds or {games_per_second} games per second')
    print(f'Win Rate: {float(wins)/num_games}')


def random_code(code_length: int, code_colors: int) -> List[int]:
    return [ random.randint(0, code_colors - 1) for i in range(code_length) ]

def init_game(agent: Agent, rules: Ruleset) -> Game:
    code = random_code(rules.num_slots, rules.num_colors)
    return Game(code, [], [], agent, rules, uid())


agents = {
    'human': HumanAgent,
    'random': RandomAgent
}

def main() -> int:
    p = argparse.ArgumentParser()
    p.add_argument('--application-data-dir', type=str, default=APPLICATION_DATA_DIR, help='directory to store game and other data')
    p.add_argument('--num-colors', type=int, default=6, help='number of colors each slot is able to occupy')
    p.add_argument('--num-slots', type=int, default=4, help='number of pegs the code is comprised of')
    p.add_argument('--num-guesses', type=int, default=10, help='number of guesses allowed before the game is lost')
    p.add_argument('--num-games', type=int, default=1, help='number of games to play before exiting')
    p.add_argument('--agent', help='agent to use', choices=agents.keys(), default='human')
    p.add_argument('label', type=str, help='name to use to record the game statistics')
    args = p.parse_args()
    print('starting mastermind simulator...')

    rules = Ruleset(
        num_colors = args.num_colors,
        num_slots = args.num_slots,
        num_guesses = args.num_guesses
    )

    if args.agent == 'human':
        agent = HumanAgent(name=args.label)
    else:
        agent = RandomAgent(name=args.label)
    run_simulation(args.num_games, rules, agent, args.application_data_dir, args.agent == 'human')


if __name__ == '__main__':
    sys.exit(main())