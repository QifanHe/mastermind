from termcolor import colored
from typing import List

from models import Response, Game

FILL_CHARACTER = '█'
FILL_WIDTH = 4
FILL_HEIGHT = 1

_PEG_COLORS = (
    'red',
    'green',
    'yellow',
    'blue',
    'magenta',
    'white'
)

_EMPTY_SLOT_COLOR = 'grey'
_BORDER_COLOR = 'grey'


def print_block(color: str, repeat: int = 1, end: str = ''):
    print(colored(''.join([FILL_CHARACTER] * repeat), color), end=end)


def print_row(row: List[int], response: Response = None, end: str = '\n'):
    for _ in range(FILL_HEIGHT):
        for c in row:
            print_block(_PEG_COLORS[c], FILL_WIDTH)
        if response:
            print(colored(FILL_CHARACTER, _BORDER_COLOR), end='')
            print(colored(f' {response.red_pins} ', 'red'), end='')
            print(colored(f' {response.white_pins} ', 'white'), end='')
            print(colored(f' {response.empty_pins} ', 'cyan'), end='')

        print('', end=end)



def _print_game(rows: List[List[int]], responses: List[Response], max_rows: int, row_length: int, code: List[int], print_code: bool = True):
    if print_code:
        print_row(code)
    print(colored(''.join([FILL_CHARACTER] * row_length), _BORDER_COLOR))
    for row_index in range(max_rows):
        if row_index >= len(rows):
            print()
        else:
            print_row(rows[row_index], responses[row_index])

def print_game(game: Game, print_code: bool = True):
    _print_game(game.guesses, game.responses, game.rules.num_guesses, game.rules.num_slots, game.code, print_code)


def print_code_colors():
    for i in range(len(_PEG_COLORS)):
        print(colored(f'{i}', _PEG_COLORS[i]), end=' ')
    print()




# Print iterations progress                                                                                                                                
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """                                                                                                                                                    
    Call in a loop to create terminal progress bar                                                                                                         
    @params:                                                                                                                                               
        iteration   - Required  : current iteration (Int)                                                                                                  
        total       - Required  : total iterations (Int)                                                                                                   
        prefix      - Optional  : prefix string (Str)                                                                                                      
        suffix      - Optional  : suffix string (Str)                                                                                                      
        decimals    - Optional  : positive number of decimals in percent complete (Int)                                                                    
        length      - Optional  : character length of bar (Int)                                                                                            
        fill        - Optional  : bar fill character (Str)                                                                                                 
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)                                                                                  
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete                                                                                                                           
    if iteration == total:
        print()